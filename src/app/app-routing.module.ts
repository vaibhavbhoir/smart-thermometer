import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TempratureComponent } from './temprature/temprature.component';
import { LoginComponent } from "./login/login.component"
import { WelcomeComponent} from "./welcome/welcome.component";
import { PhotoclickComponent } from "./photoclick/photoclick.component";
import { RegisterComponent} from "./register/register.component";
import { ThankyouComponent } from "./thankyou/thankyou.component";
import { HistoryComponent } from "./history/history.component";
import { ChangepasswordComponent } from "./changepassword/changepassword.component";
import { ForgotpasswordComponent } from "./forgotpassword/forgotpassword.component";
import { HistoricdataComponent } from "./historicdata/historicdata.component";
import { UserprofileComponent } from "./userprofile/userprofile.component";
import { UserhistoryComponent } from "./userhistory/userhistory.component";
import { UserhistorytabularComponent } from "./userhistorytabular/userhistorytabular.component";
import { PagenotfoundComponent } from "./pagenotfound/pagenotfound.component";
import { MyprofileComponent } from "./myprofile/myprofile.component";


const routes: Routes = [
  { path:"", redirectTo: "/login", pathMatch:"full"},
  { path: 'login', component: LoginComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'photoclick', component: PhotoclickComponent },
  { path: 'temprature', component: TempratureComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'thankyou', component: ThankyouComponent },
  { path: 'history', component: HistoryComponent },
  { path: 'changepassword', component: ChangepasswordComponent },
  { path: 'forgotpassword', component: ForgotpasswordComponent },
  { path: 'historicdata', component: HistoricdataComponent },
  { path: 'userprofile', component: UserprofileComponent },
  { path: 'userhistory', component: UserhistoryComponent },
  { path: 'userhistorytabular', component: UserhistorytabularComponent },
  { path: 'myprofile', component: MyprofileComponent },
  { path: '**', component: PagenotfoundComponent },







];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
