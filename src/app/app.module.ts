import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { TempratureComponent } from './temprature/temprature.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RegisterComponent } from './register/register.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { HistoryComponent } from './history/history.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { HistoricdataComponent } from './historicdata/historicdata.component';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { UserhistoryComponent } from './userhistory/userhistory.component';
import { UserhistorytabularComponent } from './userhistorytabular/userhistorytabular.component';
import { PhotoclickComponent } from './photoclick/photoclick.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { MyprofileComponent } from './myprofile/myprofile.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TempratureComponent,
    WelcomeComponent,
    SidebarComponent,
    RegisterComponent,
    ThankyouComponent,
    HistoryComponent,
    ChangepasswordComponent,
    ForgotpasswordComponent,
    HistoricdataComponent,
    UserprofileComponent,
    UserhistoryComponent,
    UserhistorytabularComponent,
    PhotoclickComponent,
    PagenotfoundComponent,
    MyprofileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
